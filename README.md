# Configuration Daemon for service discovery

## API

### Store an entry

```json
{
  "vhosts": {
    "www.foo.de": {
      "public_ip": "1.2.3.4",
    },
    "www.bar.de": {
      "public_ip": "4.3.2.1",
    },
  },
}
```

```
curl -XPUT -d@entry.json \
  http://rex-io-server/1.0/confd/production/lb/vhosts
```

### Retrieve entries

Retrieving all entries from a key:
```
curl -XGET \
  http://rex-io-server/1.0/confd/production/lb/vhosts
```

Retrieving sub entries from a key:
```
curl -XGET \
  http://rex-io-server/1.0/confd/production/lb/vhosts/www.foo.de
```

```
curl -XGET \
  http://rex-io-server/1.0/confd/production/lb/vhosts/www.foo.de/public_ip
```

### Retrieve entries in YAML format or Java Properties notation

```
curl -XGET \
  http://rex-io-server/1.0/confd/production/lb/vhosts?format=yaml
```

```
curl -XGET \
  http://rex-io-server/1.0/confd/production/lb/vhosts?format=properties
```




